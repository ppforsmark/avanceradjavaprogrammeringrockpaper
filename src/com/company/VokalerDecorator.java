package com.company;

import java.util.regex.Pattern;

public class VokalerDecorator extends PlayerDecorator{

    public VokalerDecorator(Player basePlayer) {
        this.basePlayer = basePlayer;
    }

    public int chooseMove(int input, String opponentName){

        //Need to use find - If used matcher it wants to have aeiouy at the beginning of the word.
        Pattern pattern2V = Pattern.compile("(?i)([aeiouy].*){2,}");
        Pattern pattern3V = Pattern.compile("(?i)([aeiouy].*){3,}");

        int returnInt = 0;

        if(pattern3V.matcher(opponentName).find()){
            returnInt = 1;
        }else if(pattern2V.matcher(opponentName).find()){
            returnInt = 2;
        }else{
            returnInt = 3;
        }

        basePlayer.moves.put(returnInt, basePlayer.moves.get(returnInt) + 1);
        return returnInt;
    };
}

