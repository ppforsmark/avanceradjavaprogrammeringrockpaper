package com.company;

import java.util.*;

public abstract class Player {
    String name;
    Enum type;
    int currentWins = 0;

    int totalWins = 0;
    List<Integer> placements = new ArrayList<Integer>();
    //This is a map that represents rock, paper, sizer. Will be populated with constructor in concrete players (User and Computer)
    Map<Integer,Integer> moves=new HashMap<Integer,Integer>();

    //Only chooseMove is abstract because every other method will be the same for all players.
    public abstract int chooseMove(int input, String opponentName);

    public String printPlayerStatistic(){
        String tempString = "";
        tempString += printPlayer() + "\n";

        //Vanligaste draget
        tempString += "Prefered move: ";
        if(Collections.max(moves.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getKey() == 1){
            tempString += "Rock -> " + Collections.max(moves.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getValue() + "\n";
        } else if(Collections.max(moves.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getKey() == 2){
            tempString += "Paper -> " + Collections.max(moves.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getValue() + "\n";
        }else{
            tempString += "Sizer -> " + Collections.max(moves.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getValue() + "\n";
        }

        //Antal vunna matcher
        tempString += "Total wins(Matches): " + totalWins + "\n";

        //Antal vunna turneringar
        tempString += "Total wins(Tournaments): ";
        tempString += placements.stream().filter(placement -> placement==1).count() + "\n";

        //Bästa placering
        tempString += "Best placement(Tournaments): ";
        tempString += placements.stream().min(Comparator.naturalOrder()).orElse(0) + "\n";

        //Sämsta placering
        tempString += "Worst placement(Tournaments): ";
        tempString += placements.stream().max(Comparator.naturalOrder()).orElse(0) + "\n";

        //Medelplacering
        tempString += "Average placement(Tournaments): ";
        tempString += placements.stream().mapToInt(i -> i).summaryStatistics().getAverage() + "\n";

        return tempString;
    }

    public void giveStatistic(int placement){
        totalWins += currentWins;
        placements.add(placement);
    }

    public String printPlayer(){
      return String.format("%s: %s",type,name);
    };

    public Enum getType() {
        return type;
    }

    public String getName(){
        return name;
    }

    public int getCurrentWins() {
        return currentWins;
    }

    public void addToCurrentWins(){
        currentWins++;
    }

    public void resetCurrentWins(){
        currentWins = 0;
    }
}
