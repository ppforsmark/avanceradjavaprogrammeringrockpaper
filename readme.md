Eleven skall i grupp eller individuellt utveckla ett sten-sax-påse-spel. Den fördjupande uppgiften innefattar att vidareutveckla och vid behov omstruktuera koden så att spelet:

Datormotståndarna ska ha minst 3 olika ”personligheter” – en väljer drag slumpvis, en väljer drag efter vilken minut klockan är, en väljer drag efter hur många vokaler som ingår i motståndarens namn.

Slutresultatet för varje avslutad turnering ska sparas tillsammans med tid och datum - antingen i minnet, t.ex. genom en lista, eller med hjälp av en databas.  Programmet skall sedan kunna visa statistik om olika spelares snittplacering i turneringen, bästa placering samt sämsta placering

För VG krävs därtill att programmet nyttjar sig av minst 1 etablerat designpattern. Det valda designpattern skall markeras och namnges i koden genom en kommentar.