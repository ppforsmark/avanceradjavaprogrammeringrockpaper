package com.company;

import java.time.LocalDateTime;

public class KlockaDecorator extends PlayerDecorator{

    public KlockaDecorator(Player basePlayer) {
        this.basePlayer = basePlayer;
    }

    public int chooseMove(int input, String opponentName){
        LocalDateTime ldt = LocalDateTime.now();
        int minuteNow = ldt.getMinute();

        int returnInt = 0;
        if(minuteNow%3 == 0){
            returnInt = 1;
        }else if(minuteNow%2 == 0){
            returnInt = 2;
        }else {
            returnInt = 3;
        }
        basePlayer.moves.put(returnInt, basePlayer.moves.get(returnInt) + 1);
        return returnInt;
    };
}
