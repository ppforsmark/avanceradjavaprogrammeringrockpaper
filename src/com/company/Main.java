package com.company;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Pattern;

public class Main {

    public static Scanner sc = new Scanner(System.in);
    //List of all tournaments and players
    public static List<String> tournamentList = new ArrayList<String>();
    public static List<Player> playerList = new ArrayList<Player>();

    public static void main(String[] args) throws InterruptedException {
        //I will use a combination of factorypattern and decoratorpattern.
        //The factorypattern will create the players and the decorator pattern will give the computerplayers their strategy.
        //I choose to do this to be able to create a hardmode with the same players.

        //Create a factory for players
        PlayerFactory playerFactory = new PlayerFactory();
        boolean gameLoop = true;
        String userName;

        //Start-up section -> Welcome message -> Gets players name
        System.out.println("Welcome to Rock, Paper, Sizer!");
        System.out.println("Whats your name?");
        userName = sc.nextLine();

        //With help of factoryPattern create one new userplayer - Stores to list to simplify statistic print
        Player user = playerFactory.createPlayer(PlayerType.USER, userName);

        //With help of factoryPattern create three new computerplayers - Stores to list to simplify statistic print
        Player vokalis = playerFactory.createPlayer(PlayerType.COMPUTER, "Vokalis");
        Player slumpis = playerFactory.createPlayer(PlayerType.COMPUTER, "Slumpisi");
        Player klockis = playerFactory.createPlayer(PlayerType.COMPUTER, "Klockis");

        System.out.println("----------");
        System.out.println(String.format("Welcome %s! What do you want to do?", userName));

        //GameLoop - Do while - Run at least once
        do{
            for (Player el:playerList) {
                el.resetCurrentWins();
            }

            //We will clear the playerlist so that we can decorate all players then add them again.
            playerList.clear();

            //Print menu
            System.out.println("[1] - Play a tournament -> Easy: Computer name give hints on next move)");
            System.out.println("[2] - Play a tournament -> Hard: All is random)");
            System.out.println("[3] - See statistic");
            System.out.println("[4] - Quit");
            switch (tryInput(1,4)){
                case 1:
                    playerList.add(user);
                    //Decorate computerPlayers with a playerDecorator that overrides chooseMove function.
                    //All getters/setters/printers, all that manipulates the objectdata is overridden in PlayerDecorator
                    //and references back to the base object(VokalerDecorator references volakis when changing data) which mean
                    //that even if we add or change decorator, the variables will persist.
                    playerList.add(new VokalerDecorator(vokalis));
                    playerList.add(new SlumpDecorator(slumpis));
                    playerList.add(new KlockaDecorator(klockis));

                    System.out.println("----------");
                    wait(2000, "All players, get ready! Tournament starting in 3....2....");
                    System.out.println("----------");
                    System.out.print("Match 1: ");
                    runMatch(0, 1);
                    System.out.print("Match 2: ");
                    runMatch(1, 3);
                    System.out.print("Match 3: ");
                    runMatch(0, 3);
                    System.out.print("Match 4: ");
                    runMatch(1, 2);
                    System.out.print("Match 5: ");
                    runMatch(2, 3);
                    System.out.print("Match 6: ");
                    runMatch(2, 0);
                    printAndSaveTournament();
                    break;
                case 2:
                    System.out.println("----------");
                    System.out.println("HardMode");
                    wait(2000, "All players, get ready! Tournament starting in 3....2....");
                    playerList.add(user);
                    //Decorate computerPlayers with a playerDecorator that overrides chooseMove function.
                    //All getters/setters/printers, all that manipulates the objectdata is overridden in PlayerDecorator
                    //and references back to the base object(VokalerDecorator references volakis when changing data) which mean
                    //that even if we add or change decorator, the variables will persist.
                    playerList.add(new SlumpDecorator(vokalis));
                    playerList.add(new SlumpDecorator(slumpis));
                    playerList.add(new SlumpDecorator(klockis));
                    System.out.println("----------");
                    System.out.print("Match 1: ");
                    runMatch(0, 1);
                    System.out.print("Match 2: ");
                    runMatch(1, 3);
                    System.out.print("Match 3: ");
                    runMatch(0, 3);
                    System.out.print("Match 4: ");
                    runMatch(1, 2);
                    System.out.print("Match 5: ");
                    runMatch(2, 3);
                    System.out.print("Match 6: ");
                    runMatch(2, 0);
                    printAndSaveTournament();
                    break;
                case 3:
                    System.out.println("----------");
                    System.out.println("Statistic");
                    for (String el:tournamentList) {
                        System.out.println(el);
                    }
                    System.out.println("----------");
                    System.out.println("[1] - See aggregated statistic");
                    System.out.println("[2] - Go Back");
                    switch (tryInput(1,2)){
                        case 1:
                            System.out.println("Aggregated statistic");
                            //Because of clear - We need to re-add the players to the list. Could have choose to print only user, and computer etc. but I want to work with list
                            //as I do that everywhere in the program.
                            playerList.add(user);
                            playerList.add(new VokalerDecorator(vokalis));
                            playerList.add(new SlumpDecorator(slumpis));
                            playerList.add(new KlockaDecorator(klockis));
                            System.out.println();
                            for (Player el:playerList) {
                                System.out.println(el.printPlayerStatistic());
                                System.out.println();
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case 4:
                    //End game
                    gameLoop = false;
                    break;
                default:
                    //Hopefully we'll never see this message
                    System.out.println("Something went wrong, ending the game");
                    System.out.println("Please copy and send to support: Switch1 got case:Default. Broken switch or tryInput()");
                    gameLoop = false;
                    break;
            }
        }while (gameLoop);

        //Goodbye message
        System.out.println("----------");
        System.out.println(String.format("Thanks for playing %s! Hope to see you soon.", userName));
    }

    public static void printAndSaveTournament() throws InterruptedException {
        String tournamentString = "";
        //Sorts player with winner first.
        List<Player> tempPlayerList = playerList.stream().sorted(Comparator.comparing(Player::getCurrentWins).reversed()).toList();

        //Gives player statistic based on placement
        int placement = 1;
        for (Player el:tempPlayerList) {
            playerList.get(playerList.indexOf(el)).giveStatistic(placement);
            placement++;
        }

        //Saves the tournament to a string
        LocalDateTime ldt = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM-yy HH:mm:ss");
        tournamentString += ldt.format(dtf) + "\n";
        int count = 1;
        for (Player el:tempPlayerList) {
            tournamentString += count + ". " + el.getName() + "(" + el.getCurrentWins() + "W) \n";
            count++;
        }
        System.out.println("----------");
        System.out.println("Results:");
        System.out.println(tournamentString);
        tournamentList.add(tournamentString);
        wait(1000, "");
    }

    public static void runMatch(int p1index, int p2index) throws InterruptedException {
        boolean draw = false;

        do {
            draw = false;
            System.out.println(playerList.get(p1index).printPlayer() + " -VS- " + playerList.get(p2index).printPlayer());

            int p1choice;
            int p2choice;
            //Check if players is computer or user
            if (playerList.get(p1index).getType().equals(PlayerType.USER)) {
                p1choice = playerList.get(p1index).chooseMove(getUserChoice(), playerList.get(p2index).getName());
            } else {
                p1choice = playerList.get(p1index).chooseMove(0, playerList.get(p2index).getName());
            }

            if (playerList.get(p2index).getType().equals(PlayerType.USER)) {
                p2choice = playerList.get(p2index).chooseMove(getUserChoice(), playerList.get(p1index).getName());
            } else {
                p2choice = playerList.get(p2index).chooseMove(0, playerList.get(p1index).getName());
            }

            System.out.println("----------");
            System.out.println(playerList.get(p1index).getName() + " choose " + printChoice(p1choice));
            System.out.println(playerList.get(p2index).getName() + " choose " + printChoice(p2choice));

            //Based on p1 and p2choice. Decide the winner
            switch (p1choice) {
                case 1:
                    switch (p2choice) {
                        case 2:
                            System.out.println(playerList.get(p2index).printPlayer() + " WON!");
                            playerList.get(p2index).addToCurrentWins();
                            break;
                        case 3:
                            System.out.println(playerList.get(p1index).printPlayer() + " WON!");
                            playerList.get(p1index).addToCurrentWins();
                            break;
                        default:
                            System.out.println("DRAW!");
                            System.out.println("Play Again!");
                            draw = true;
                            break;
                    }
                    break;
                case 2:
                    switch (p2choice) {
                        case 1:
                            System.out.println(playerList.get(p1index).printPlayer() + " WON!");
                            playerList.get(p1index).addToCurrentWins();
                            break;
                        case 3:
                            System.out.println(playerList.get(p2index).printPlayer() + " WON!");
                            playerList.get(p2index).addToCurrentWins();
                            break;
                        default:
                            System.out.println("DRAW!");
                            System.out.println("Play Again!");
                            draw = true;
                            break;
                    }
                    break;
                case 3:
                    switch (p2choice) {
                        case 1:
                            System.out.println(playerList.get(p2index).printPlayer() + " WON!");
                            playerList.get(p2index).addToCurrentWins();
                            break;
                        case 2:
                            System.out.println(playerList.get(p1index).printPlayer() + " WON!");
                            playerList.get(p1index).addToCurrentWins();
                            break;
                        default:
                            System.out.println("DRAW!");
                            System.out.println("Play Again!");
                            draw = true;
                            break;
                    }
                    break;
                default:
                    break;
            }
            wait(2000, "----------");
        } while (draw);
    }

    //Gives us the string representation of 1,2 or 3
    public static String printChoice(int intChoice){
        return switch (intChoice){
            case 1 -> "Rock";
            case 2 -> "Paper";
            case 3 -> "Sizer";
            default -> "Something went wrong with printChoice()";
        };
    }

    //When user - Run this method so we can choose move
    public static int getUserChoice(){
        System.out.println("[1] - Rock");
        System.out.println("[2] - Paper");
        System.out.println("[3] - Sizer");

        return switch (tryInput(1,3)) {
            case 1 -> 1;
            case 2 -> 2;
            case 3 -> 3;
            default -> 0;
        };
    }

    //Method: Params: An OK-Input-range -> Tests if user input is OK based on type and range -> If OK-Returns inputInt.
    public static int tryInput(int startRange, int endRange){
        String tempPattern = String.format("[%s-%s]",startRange,endRange);
        Pattern pattern = Pattern.compile(tempPattern);
        boolean correctInput = false;
        int tempInt = 0;

        while (!correctInput) {
            try {
                tempInt = sc.nextInt();
                if(!pattern.matcher(Integer.toString(tempInt)).matches()){
                    throw new IllegalArgumentException("Not a regex match");
                }
                correctInput = true;
            } catch (Exception e) {
                System.out.println(String.format("Wrong input, try again. Enter a number between %s and %s.", startRange, endRange));
                //Collects \n
                sc.nextLine();
            }
        }
        //Collects \n
        return tempInt;
    }

    //Method: Params: Wait-time and message -> Creates a thread with sleep. Prints a message. Runs the thread -> Pauses the game until Join.
    public static void wait(long millis, String message) throws InterruptedException {
        Thread thread1 = new Thread(() ->{
            try {
                System.out.println(message);
                Thread.sleep(millis);
            }catch (Exception e){
                e.printStackTrace();
            }
        });
        thread1.start();
        thread1.join();
    }

}
