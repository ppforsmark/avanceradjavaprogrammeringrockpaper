package com.company;

public class PlayerFactory {
    public Player createPlayer(PlayerType playerType, String name) {
        return switch (playerType) {
            case COMPUTER -> new Computer(name);
            case USER -> new User(name);
        };
    }
}
