package com.company;

public class Computer extends Player{
    public Computer(String name) {
        this.name = name;
        this.type = PlayerType.COMPUTER;
        this.moves.put(1,0);
        this.moves.put(2,0);
        this.moves.put(3,0);
    }

    //Method is default and will never be used. Decorator will handle chooseMove.
    @Override
    public int chooseMove(int input, String opponentName){
        this.moves.put(input, moves.get(input) + 1);
        return switch (input) {
            case 1 -> 1;
            case 2 -> 2;
            case 3 -> 3;
            default -> 0;
        };
    };
}
