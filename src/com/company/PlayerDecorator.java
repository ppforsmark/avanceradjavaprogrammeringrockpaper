package com.company;

import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

//Sam as PlayerClass but instead of manipulating itself the decorator will manipulate the basePlayer that is stored.
public abstract class PlayerDecorator extends Player{
    Player basePlayer;

    public abstract int chooseMove(int input, String opponentName);

    @Override
    public String printPlayer() {
        return basePlayer.printPlayer();
    }

    @Override
    public Enum getType() {
        return basePlayer.getType();
    }

    @Override
    public String getName() {
        return basePlayer.getName();
    }

    @Override
    public int getCurrentWins() {
        return basePlayer.getCurrentWins();
    }

    @Override
    public void addToCurrentWins() {
        basePlayer.addToCurrentWins();
    }

    @Override
    public void resetCurrentWins() {
        basePlayer.resetCurrentWins();
    }

    @Override
    public void giveStatistic(int placement) {
        basePlayer.totalWins += basePlayer.currentWins;
        basePlayer.placements.add(placement);
    }

    @Override
    public String printPlayerStatistic() {
        String tempString = "";
        tempString += basePlayer.printPlayer() + "\n";

        //Vanligaste draget
        tempString += "Prefered move: ";
        if(Collections.max(basePlayer.moves.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getKey() == 1){
            tempString += "Rock -> " + Collections.max(basePlayer.moves.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getValue() + "\n";
        } else if(Collections.max(basePlayer.moves.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getKey() == 2){
            tempString += "Paper -> " + Collections.max(basePlayer.moves.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getValue() + "\n";
        }else{
            tempString += "Sizer -> " + Collections.max(basePlayer.moves.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getValue() + "\n";
        }

        //Antal vunna matcher
        tempString += "Total wins(Matches): " + basePlayer.totalWins + "\n";

        //Antal vunna turneringar
        tempString += "Total wins(Tournaments): ";
        tempString += basePlayer.placements.stream().filter(placement -> placement==1).count() + "\n";

        //Bästa placering
        tempString += "Best placement(Tournaments): ";
        tempString += basePlayer.placements.stream().min(Comparator.naturalOrder()).orElse(0) + "\n";

        //Sämsta placering
        tempString += "Worst placement(Tournaments): ";
        tempString += basePlayer.placements.stream().max(Comparator.naturalOrder()).orElse(0) + "\n";

        //Medelplacering
        tempString += "Average placement(Tournaments): ";
        tempString += basePlayer.placements.stream().mapToInt(i -> i).summaryStatistics().getAverage() + "\n";

        return tempString;
    }
}
