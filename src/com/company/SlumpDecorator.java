package com.company;

import java.util.Random;

public class SlumpDecorator extends PlayerDecorator{

    public SlumpDecorator(Player basePlayer) {
        this.basePlayer = basePlayer;
    }

    public int chooseMove(int input, String opponentName){
        int random = (int)Math.floor((Math.random()*3) + 1);
        basePlayer.moves.put(random, basePlayer.moves.get(random) + 1);
        return random;
    };
}
